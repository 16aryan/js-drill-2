// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
const inventory = require('../inventory');

function problem3(inventory){
    if(!inventory || typeof inventory!= 'object'){
        return console.log('please provide inventory');
    }
    
    let carData = inventory.map((inventory) => inventory.car_model.toLowerCase());
  
   sortedData=carData.sort();
    console.log(sortedData);
}

//problem3(inventory);
module.exports = problem3;