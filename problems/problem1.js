
// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"*"
const inventory = require("../inventory")
if(!inventory || typeof inventory!= 'object'){
    return console.log('please provide inventory');
}

function problem1(inventory){
 let cars = inventory.filter((inventory) => inventory.id === 33);
 return console.log(`Car 33 is a ${cars[0].car_year} ${cars[0].car_make} ${cars[0].car_model}`);

}

module.exports = problem1;
