
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.



const problem4 = require('./problem4');

const inventory = require('../inventory');

function problem5(inventory){
    if(!inventory || typeof inventory!= 'object'){
        return console.log('please provide inventory');
    }
    
    let cars = inventory.filter((inventory) => inventory.car_year <2000);
    return console.log(cars, cars.length);
}

//problem5(inventory,carYear);
module.exports = problem5;