
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the consol
const inventory = require('../inventory');

function problem6(inventory){
  if(!inventory || typeof inventory!= 'object'){
    return console.log('please provide inventory');
}

    let cars = inventory.filter((inventory) => inventory.car_make === 'Audi' || inventory.car_make==='BMW' ||inventory.car_make === 'Audi');
 console.log(JSON.stringify(cars)); 
}

//getCarName(inventory);

module.exports = problem6;