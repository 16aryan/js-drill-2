const inventory = require('../inventory');
// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"


function problem2(inventory){
    if(!inventory || typeof inventory!= 'object'){
        return console.log('please provide inventory');
    }
    
    let lastCar = inventory.slice(inventory.length-1);
//console.log(lastCar);
    return console.log(`Last car is a ${lastCar[0].car_make} ${lastCar[0].car_model} `)
}
module.exports = problem2;